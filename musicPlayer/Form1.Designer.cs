﻿namespace musicPlayer
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainForm));
            this._play = new System.Windows.Forms.Button();
            this._pause = new System.Windows.Forms.Button();
            this._stop = new System.Windows.Forms.Button();
            this._next = new System.Windows.Forms.Button();
            this._selfile = new System.Windows.Forms.OpenFileDialog();
            this._choose = new System.Windows.Forms.Button();
            this._currenttrack = new System.Windows.Forms.Label();
            this._albumart = new System.Windows.Forms.PictureBox();
            this._songtime = new System.Windows.Forms.Label();
            this._timeelapsed = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._progress = new System.Windows.Forms.ProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this._albumart)).BeginInit();
            this.SuspendLayout();
            // 
            // _play
            // 
            this._play.Location = new System.Drawing.Point(12, 12);
            this._play.Name = "_play";
            this._play.Size = new System.Drawing.Size(127, 50);
            this._play.TabIndex = 1;
            this._play.Text = "Play";
            this._play.UseVisualStyleBackColor = true;
            this._play.Click += new System.EventHandler(this._play_Click);
            // 
            // _pause
            // 
            this._pause.Location = new System.Drawing.Point(12, 68);
            this._pause.Name = "_pause";
            this._pause.Size = new System.Drawing.Size(127, 50);
            this._pause.TabIndex = 2;
            this._pause.Text = "Pause";
            this._pause.UseVisualStyleBackColor = true;
            this._pause.Click += new System.EventHandler(this._pause_Click);
            // 
            // _stop
            // 
            this._stop.Location = new System.Drawing.Point(12, 124);
            this._stop.Name = "_stop";
            this._stop.Size = new System.Drawing.Size(127, 50);
            this._stop.TabIndex = 3;
            this._stop.Text = "Stop";
            this._stop.UseVisualStyleBackColor = true;
            this._stop.Click += new System.EventHandler(this._stop_Click);
            // 
            // _next
            // 
            this._next.Location = new System.Drawing.Point(12, 180);
            this._next.Name = "_next";
            this._next.Size = new System.Drawing.Size(127, 50);
            this._next.TabIndex = 4;
            this._next.Text = "Next";
            this._next.UseVisualStyleBackColor = true;
            // 
            // _selfile
            // 
            this._selfile.Filter = "\"Music (.mp3)|*.mp3|ALL Files (*.*)|*.*\"";
            this._selfile.Title = "Pick song...";
            this._selfile.FileOk += new System.ComponentModel.CancelEventHandler(this._selfile_FileOk);
            // 
            // _choose
            // 
            this._choose.Location = new System.Drawing.Point(12, 234);
            this._choose.Name = "_choose";
            this._choose.Size = new System.Drawing.Size(127, 23);
            this._choose.TabIndex = 5;
            this._choose.Text = "Choose song...";
            this._choose.UseVisualStyleBackColor = true;
            this._choose.Click += new System.EventHandler(this._choose_Click);
            // 
            // _currenttrack
            // 
            this._currenttrack.AutoSize = true;
            this._currenttrack.Location = new System.Drawing.Point(145, 219);
            this._currenttrack.Name = "_currenttrack";
            this._currenttrack.Size = new System.Drawing.Size(131, 13);
            this._currenttrack.TabIndex = 7;
            this._currenttrack.Text = "Currently playing: (nothing)";
            // 
            // _albumart
            // 
            this._albumart.Image = global::musicPlayer.Properties.Resources._5206_200;
            this._albumart.Location = new System.Drawing.Point(145, 12);
            this._albumart.Name = "_albumart";
            this._albumart.Size = new System.Drawing.Size(200, 200);
            this._albumart.TabIndex = 6;
            this._albumart.TabStop = false;
            // 
            // _songtime
            // 
            this._songtime.AutoSize = true;
            this._songtime.Location = new System.Drawing.Point(105, 273);
            this._songtime.Name = "_songtime";
            this._songtime.Size = new System.Drawing.Size(34, 13);
            this._songtime.TabIndex = 8;
            this._songtime.Text = "00:00";
            // 
            // _timeelapsed
            // 
            this._timeelapsed.AutoSize = true;
            this._timeelapsed.Location = new System.Drawing.Point(105, 260);
            this._timeelapsed.Name = "_timeelapsed";
            this._timeelapsed.Size = new System.Drawing.Size(34, 13);
            this._timeelapsed.TabIndex = 9;
            this._timeelapsed.Text = "00:00";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 260);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Elapsed time:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 273);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Total song time:";
            // 
            // _progress
            // 
            this._progress.Location = new System.Drawing.Point(148, 260);
            this._progress.Name = "_progress";
            this._progress.Size = new System.Drawing.Size(197, 26);
            this._progress.TabIndex = 12;
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(357, 312);
            this.Controls.Add(this._progress);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._timeelapsed);
            this.Controls.Add(this._songtime);
            this.Controls.Add(this._currenttrack);
            this.Controls.Add(this._albumart);
            this.Controls.Add(this._choose);
            this.Controls.Add(this._next);
            this.Controls.Add(this._stop);
            this.Controls.Add(this._pause);
            this.Controls.Add(this._play);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "mainForm";
            this.Text = "csMusic v0.2.1b";
            ((System.ComponentModel.ISupportInitialize)(this._albumart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button _play;
        private System.Windows.Forms.Button _pause;
        private System.Windows.Forms.Button _stop;
        private System.Windows.Forms.Button _next;
        private System.Windows.Forms.OpenFileDialog _selfile;
        private System.Windows.Forms.Button _choose;
        private System.Windows.Forms.PictureBox _albumart;
        private System.Windows.Forms.Label _currenttrack;
        private System.Windows.Forms.Label _songtime;
        private System.Windows.Forms.Label _timeelapsed;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ProgressBar _progress;
    }
}

