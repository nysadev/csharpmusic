﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NAudio;
using NAudio.Wave;

namespace musicPlayer
{
    public partial class mainForm : Form
    {
        public IWavePlayer wavePlayer;
        public AudioFileReader audioFileReader;
        private string fileName;
        private string userpath;
        private bool isPlaying = false;
        Timer timer1 = new Timer();

        public mainForm()
        {
            InitializeComponent();
            timer1.Interval = 1000;
            timer1.Tick += onTimerTick;

        }

        private void _selfile_FileOk(object sender, CancelEventArgs e)
        {
            fileName = _selfile.FileName;
            userpath = _selfile.SafeFileName;
            _currenttrack.Text = "Currently playing: " + userpath;
            Update();

            var file = TagLib.File.Create(fileName);
            if (file.Tag.Pictures.Length >= 1)
            {
                var bin = (byte[])(file.Tag.Pictures[0].Data.Data);
                _albumart.Image = Image.FromStream(new MemoryStream(bin)).GetThumbnailImage(200, 200, null, IntPtr.Zero);
            }
        }

        private static string formatTimeSpan(TimeSpan ts)
        {
            return string.Format("{0:D2}:{1:D2}", (int)ts.TotalMinutes, ts.Seconds);
        }
        private static int formatTsInt(TimeSpan ts)
        {
            return (int)ts.TotalSeconds;
        }
        void onTimerTick(object sender, EventArgs e)
        {
            if(audioFileReader != null)
            {
                _timeelapsed.Text = formatTimeSpan(audioFileReader.CurrentTime);
                _songtime.Text = formatTimeSpan(audioFileReader.TotalTime);
                _progress.Maximum = formatTsInt(audioFileReader.TotalTime);
                _progress.Value++;
            }
        }

        private void _play_Click(object sender, EventArgs e)
        {
            if (fileName == null)
            {
                _selfile.ShowDialog();
            }
            if (fileName != null)
            {
                if (isPlaying == false)
                {
                    beginPlayback();
                }
                else
                {
                    wavePlayer.Play();
                    timer1.Enabled = true;
                }

            }
        }

        private void beginPlayback()
        {
            wavePlayer = new WaveOut();
            audioFileReader = new AudioFileReader(fileName);
            wavePlayer.Init(audioFileReader);
            wavePlayer.Play();
            wavePlayer.PlaybackStopped += endOfSong;
            _progress.Maximum = formatTsInt(audioFileReader.TotalTime);
            Console.WriteLine(formatTsInt(audioFileReader.TotalTime));
            timer1.Enabled = true;
            isPlaying = true;
        }

        private void cleanUp()
        {
            if(audioFileReader != null)
            {
                audioFileReader.Dispose();
                audioFileReader = null;
            }
            if(wavePlayer != null)
            {
                wavePlayer.Dispose();
                wavePlayer = null;
            }
        }

        private void _pause_Click(object sender, EventArgs e)
        {
            if (wavePlayer != null)
            {
                timer1.Enabled = false;
                wavePlayer.Pause();
            }
        }

        private void _choose_Click(object sender, EventArgs e)
        {
            _selfile.ShowDialog();
        }

        private void _stop_Click(object sender, EventArgs e)
        {
            if (wavePlayer != null)
            {
                wavePlayer.Stop();
                cleanUp();
                timer1.Enabled = false;
                _timeelapsed.Text = "00:00";
                _songtime.Text = "00:00";
                _currenttrack.Text = "Currently playing: (nothing)";
                _albumart.Image = Properties.Resources._5206_200;
                fileName = null;
                isPlaying = false;
            }
        }
        private void endOfSong(object sender, EventArgs e)
        {
            if (wavePlayer != null)
            {
                wavePlayer.Stop();
                cleanUp();
                timer1.Enabled = false;
                _timeelapsed.Text = "00:00";
                _songtime.Text = "00:00";
                _currenttrack.Text = "Currently playing: (nothing)";
                _albumart.Image = Properties.Resources._5206_200;
                fileName = null;
                isPlaying = false;
            }
        }
    }
}
