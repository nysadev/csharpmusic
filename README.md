# csMusic (beta)
A music player written in C# using NAudio and TagLibSharp, using Costura for single exe compiling.
Current features inclue basic play, pause, and stop functionality, a graphical and text base song time system, and displaying of album art.
Features planned for the future: playlist functionality, playing albums, scroll bar for music time, etc.

Latest release: 0.2.1b
Find the latest release: https://bitbucket.org/nysadev/csharpmusic/downloads/